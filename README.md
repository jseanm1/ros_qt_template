Setup Instructions : 
(Assumes you have setup ROS Melodic and QT5 on Ubuntu 18.04)

Install QT build
	sudo apt-get install ros-melodic-qt-build

Build
	catkin build

Run
	roscore
	rosrun ros_qt_demo ros_qt_demo	
