#include <QApplication>

#include <ros_qt_demo/main_window.hpp>

#ifndef THREAD_INCLUDED_H
#define THREAD_INCLUDED_H
#include <boost/lockfree/queue.hpp>
#include <boost/thread.hpp>
#endif

#include <iostream>

class GuiController {
	public :
		GuiController(int argc, char** argv, ros::NodeHandle& nh) :
			argc_(argc),
			argv_(argv),
			nh_(nh) {}

		void thread_entry() {
			QApplication app(argc_, argv_);
			ros_qt_demo::MainWindow w(argc_, argv_, nh_);
			w.show();
			app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
			int result = app.exec();
		}

	private :
		int argc_;
		char** argv_;
		ros::NodeHandle& nh_;
};

int main(int argc, char** argv) {
	ros::init(argc, argv, "ros_qt_demo_node");
	ros::NodeHandle nh;

	GuiController guiController(argc, argv, nh);

	boost::thread guiThread(&GuiController::thread_entry, guiController);

	ros::spin();

	std::cout << "Exiting ROS spin" << std::endl;

	std::cout << "Wainting for GUI thread" << std::endl;
	guiThread.join();
	std::cout << "Exiting app" << std::endl;

	return 0;
}