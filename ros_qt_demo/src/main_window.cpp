#include <ros_qt_demo/main_window.hpp>

#include <iostream>

namespace ros_qt_demo {

using namespace Qt;

MainWindow::MainWindow(int argc, char** argv, ros::NodeHandle &nh, QWidget *parent)
	: QMainWindow(parent),
	nh_(nh) {
		ui.setupUi(this);

		setupSubsNPubs();

		connectSLOTS();
	  }

MainWindow::~MainWindow() {
	ros::shutdown();
	std::cout << "Exiting Main Window" << std::endl;
}

void MainWindow::setupSubsNPubs() {
	sub_ = nh_.subscribe("/data", 1, &MainWindow::subscriberCb, this);
	pub_ = nh_.advertise<std_msgs::Int64>("/data", 1);
}

void MainWindow::connectSLOTS() {
	connect(ui.button_publish, SIGNAL(clicked()), this, SLOT(publishButtonClicked()));
}

void MainWindow::publishButtonClicked() {
	std_msgs::Int64 msg;

	msg.data = ui.spinBox_published_data->value();

	pub_.publish(msg);
}

void MainWindow::subscriberCb(const std_msgs::Int64::ConstPtr& msg) {
	ui.label_subscribed_data->setNum(int(msg->data));
}

}


