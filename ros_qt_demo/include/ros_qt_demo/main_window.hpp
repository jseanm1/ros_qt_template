#ifndef ROS_QT_DEMO_MAIN_WINDOW_HPP
#define ROS_QT_DEMO_MAIN_WINDOW_HPP

#include <QApplication>
#include <QWidget>

#include "ui_main_window.h"

#include <ros/ros.h>
#include <std_msgs/Int64.h>

namespace ros_qt_demo {

class MainWindow : public QMainWindow {
Q_OBJECT

public :
	MainWindow(int, char**, ros::NodeHandle&, QWidget *parent = 0);
	~MainWindow();

	Ui::MainWindow ui;

private :
	ros::NodeHandle& nh_;
	ros::Subscriber sub_;
	ros::Publisher pub_;

	void subscriberCb(const std_msgs::Int64::ConstPtr&);

	void setupSubsNPubs();
	void connectSLOTS();

private slots :
	void publishButtonClicked();

};

}

#endif